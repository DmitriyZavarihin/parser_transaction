-- Adminer 4.8.1 PostgreSQL 16.0 (Debian 16.0-1.pgdg120+1) dump

CREATE SEQUENCE all_transactions_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."all_transactions" (
    "id" integer DEFAULT nextval('all_transactions_id_seq') NOT NULL,
    "time" character varying(42),
    "value" double precision,
    "name_token" character varying(6),
    "balance" double precision,
    "from_vallet" character varying(42),
    "to_vallet" character varying(42),
    "metod" character varying(42),
    "transaction_id" character varying(100),
    "rate" character varying(10),
    "currency" character varying(20),
    "purpose" character varying(20),
    "own_adress" character varying(42),
    "blockchain" character varying(42),
    "block_timestamp" character varying(30),
    "bot_set" character varying(100),
    CONSTRAINT "all_transactions_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


CREATE SEQUENCE settings_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."settings" (
    "id" integer DEFAULT nextval('settings_id_seq') NOT NULL,
    "wallet" character varying(42),
    "comment" character varying(30),
    "group" character varying(42),
    "user" character varying(42),
    "telegram_username" character varying(42),
    "name_list" character varying(42),
    "balance" double precision,
    "cash" double precision,
    "id_telegram" character varying(30),
    "last_deal" integer,
    "next_" integer,
    CONSTRAINT "settings_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "settings" ("id", "wallet", "comment", "group", "user", "telegram_username", "name_list", "balance", "cash", "id_telegram", "last_deal", "next_") VALUES
(1,	'TY4fg6DAQHMC3LdbPjp2FcK5J4soXxwose',	'юзер',	'TRC',	'DIMt',	'@SevenXXXX',	'DIMt_wose',	0,	0,	'2/20XXXXXXXX',	0,	0),
(2,	'TN7pYfB9SyAcQ8PiWwiqSdXQoQ1BoWsdYM',	'юзер',	'TRC',	'MZN',	'@SevenXXXX',	'MZNt_sdYM',	0,	0,	'2/20XXXXXXXX',	0,	0),
(3,	'TF7C43DgKHrpzfiuy8kkPqoQRRRmzRLBV9',	'юзер',	'TRC',	'AHM',	'@SevenXXXX',	'AHMt_LBV9',	0,	0,	'2/20XXXXXXXX',	0,	0),
(4,	'TDxXEqyhBpQwzbu5h5QZwGMtyC7PE6udox',	'юзер',	'TRC',	'CLN',	'@SevenXXXX',	'MARt_udox',	0,	0,	'2/20XXXXXXXX',	0,	0),
(5,	'TAFhoeiraaeLQmwX85LCzSZ6WU3xqWx7Sc',	'юзер',	'TRC',	'MAR',	'@SevenXXXX',	'CLNt_x7Sc',	0,	0,	'2/20XXXXXXXX',	0,	0),
(6,	'0x880036f88E9CEA8f27353844a56d309B01Da8741',	'юзер',	'ERC',	'DIM',	'@SevenXXXX',	'DIMe_8741',	0,	0,	'2/20XXXXXXXX',	0,	0),
(7,	'0xc6C5a3e4CF9dea5adb20D9503Fa763c6ebc0Ef87',	'юзер',	'ERC',	'MZN',	'@SevenXXXX',	'MZNe_Ef87',	0,	0,	'2/20XXXXXXXX',	0,	0),
(8,	'0x87427cbBc8134f395b4E8Dc80062387084CD6033',	'юзер',	'ERC',	'AHM',	'@SevenXXXX',	'AHMe_6033',	0,	0,	'2/20XXXXXXXX',	0,	0),
(9,	'0x8747F5FAd2E34be021C55FEA70c233b380d9B5C5',	'юзер',	'ERC',	'CLN',	'@SevenXXXX',	'MARe_B5C5',	0,	0,	'2/20XXXXXXXX',	0,	0),
(10,	'0x661A35115dD1220D18B5Bcb981010678eDbCc1bd',	'юзер',	'ERC',	'MAR',	'@SevenXXXX',	'CLNe_c1bd',	0,	0,	'2/20XXXXXXXX',	0,	0),
(11,	'TMFMsvZja8oWBMRen3UoVWfc3PN8XfKorR',	'pos Master',	'TRC',	'POS',	'@SevenXXXX',	'70_POS_orR',	0,	0,	'2/20XXXXXXXX',	0,	0);

-- 2023-11-13 15:49:30.027026+00
