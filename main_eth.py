#!/usr/bin/python

from parser_eth import parser_while_eth
from parser_bnb import parser_while_bnb
from sql_alchemy import cread_table, cend_message_all_transactions, commit_message, proverka, updete_settings
from get_settings import valid_adress
import time
import asyncio
from datetime import datetime


async def main():
    trx, eth, bnb = await valid_adress()
    await cread_table()
    print(len(eth))
    while True:
        try:
            if eth != []:
                start_time = datetime.now()
                await parser_while_eth(eth)
                print(datetime.now() - start_time, 'eth')
            if bnb != []:
                start_time = datetime.now()
                await parser_while_bnb(bnb)
                print(datetime.now() - start_time, 'bnb')
        except:
            continue


if __name__ == '__main__':
    asyncio.run(main())
