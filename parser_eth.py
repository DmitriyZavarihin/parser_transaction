from sql_alchemy import cread_table, cend_message_all_transactions, commit_message, proverka, updete_settings
from datetime import datetime
import time
import asyncio
from aiohttp import ClientSession
from get_settings import valid_adress


async def parser(adress):
    data = await multi_recuest(adress)
    balance = 0
    for i in range(len(data["result"])):
        transaction_id = data["result"][i]["hash"]
        block_timestamp = data["result"][i]["timeStamp"]
        from_vallet = data["result"][i]["from"]
        to_vallet = data["result"][i]["to"]
        value = int(data["result"][i]["value"]) / 1000000
        time = f'{str(datetime.fromtimestamp(float(str(data["result"][i]["timeStamp"])) + 14400))}+04:00'
        usdt = data["result"][i]["contractAddress"]
        own_adress = adress
        if own_adress.lower() == from_vallet.lower():
            metod = "sending"
        else:
            metod = "admission"
        if metod == "admission":
            balance = round(balance + value, 2)
        else:
            balance = round(balance - value, 2)
        if balance <= 0:
            balance = 0
        blockchain = 'eth'
        name_token = 'usdt'
        rate = ''
        bot_set = ''
        currency = ''
        purpose = ''
        if value <= 0:
            value = 0
        if await proverka(transaction_id, metod) and usdt == "0xdac17f958d2ee523a2206206994597c13d831ec7":
            await cend_message_all_transactions(time, value, name_token, balance, from_vallet, to_vallet, metod, transaction_id, rate, currency, purpose, own_adress, blockchain, block_timestamp, bot_set)
            await updete_settings(adress)
            print(time, value, name_token, balance, from_vallet, to_vallet, metod, transaction_id, rate, currency, purpose, own_adress, blockchain, block_timestamp, bot_set)


async def multi_recuest(adress):
    url = (f'https://api.etherscan.io/api?module=account&action='
           f'tokentx&contractaddress=0xdAC17F958D2ee523a2206206994597C13D831ec7&address='
           f'{adress}&page=0&offset=10000&startblock=0&endblock=95709126&sort=asc&apikey='
           f'CV8MYHYN3R99W2X4CW1MKR1TQGRY1S9WSY')
    async with ClientSession() as session:
        async with session.get(url, ssl=False) as f:
            data = await f.json()
    return data


async def parser_while_eth(adress):
    x = 0
    y = 0
    for i in adress:
        x = x + 1
        await parser(i)
        #await commit_message()
        print(x)



