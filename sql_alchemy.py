import os
from dotenv import load_dotenv
from sqlalchemy import Table, Column, Integer, String, Float, MetaData, ForeignKey, create_engine, select, update, LargeBinary
from sqlalchemy.orm import sessionmaker, declarative_base
import asyncio
from aiohttp import ClientSession

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

db_user = os.environ.get("POSTGRES_USER")
db_password = os.environ.get("POSTGRES_PASSWORD")
db_host = os.environ.get("POSTGRES_HOST")
db = os.environ.get("POSTGRES_DB")


engine = create_engine(f"postgresql://{db_user}:{db_password}@{db_host}/{db}")

Base = declarative_base()
Session = sessionmaker(bind=engine)
session = Session()


class User(Base):
    __tablename__ = 'all_transactions'
    id = Column(Integer, primary_key=True)
    time = Column(String(42))
    value = Column(Float)
    name_token = Column(String(6))
    balance = Column(Float)
    from_vallet = Column(String(42))
    to_vallet = Column(String(42))
    metod = Column(String(42))
    transaction_id = Column(String(100))
    rate = Column(String(10))
    currency = Column(String(20))
    purpose = Column(String(20))
    own_adress = Column(String(42))
    blockchain = Column(String(42))
    block_timestamp = Column(String(30))
    bot_set = Column(String(100))


    def __init__(self, time, value, name_token, balance, from_vallet, to_vallet, metod, transaction_id, rate, currency, purpose, own_adress, blockchain, block_timestamp, bot_set):
        self.time = time
        self.value = value
        self.name_token = name_token
        self.balance = balance
        self.from_vallet = from_vallet
        self.to_vallet = to_vallet
        self.metod = metod
        self.transaction_id = transaction_id
        self.rate = rate
        self.currency = currency
        self.purpose = purpose
        self.own_adress = own_adress
        self.blockchain = blockchain
        self.block_timestamp = block_timestamp
        self.bot_set = bot_set


class Settings(Base):
    __tablename__ = 'settings'
    id = Column(Integer, primary_key=True)
    wallet = Column(String(42))
    comment = Column(String(30))
    group = Column(String(42))
    user = Column(String(42))
    telegram_username = Column(String(42))
    name_list = Column(String(42))
    balance = Column(Float)
    cash = Column(Float)
    id_telegram = Column(String(30))
    last_deal = Column(Integer)
    next_ = Column(Integer)


    def __init__(self, wallet, comment, group, user, telegram_username, name_list, balance, cash, id_telegram, last_deal, next_):
        self.wallet = wallet
        self.comment = comment
        self.group = group
        self.user = user
        self.telegram_username = telegram_username
        self.name_list = name_list
        self.balance = balance
        self.cash = cash
        self.id_telegram = id_telegram
        self.last_deal = last_deal
        self.next_ = next_


async def cread_table():
    # Создание таблицы
    Base.metadata.create_all(engine)


async def cend_message_all_transactions(time, value, name_token, balance, from_vallet, to_vallet, metod, transaction_id, rate, currency, purpose, own_adress, blockchain, block_timestamp, bot_set):
    # запись в таблицу
    session = Session()
    try:
        session.add(User(time, value, name_token, balance, from_vallet, to_vallet, metod, transaction_id, rate, currency, purpose, own_adress, blockchain, block_timestamp, bot_set))
        session.commit()
    except:
        session.rollback()
        print("Ошибка добавления в БД")
    session.close()


async def cend_message_settings(wallet, comment, group, user, telegram_username, name_list, balance, cash, id_telegram, last_deal, next_):
    # запись в таблицу settings
    session = Session()
    try:
        session.add(Settings(wallet, comment, group, user, telegram_username, name_list, balance, cash, id_telegram, last_deal, next_))
        session.commit()
    except:
        session.rollback()
        print("Ошибка добавления в БД")
    session.close()


async def commit_message():
    # коммит в таблицу
    session.commit()


async def proverka(transaction_id, metod):
    # проверка на повтор transaction_id
    session = Session()
    if session.query(User).filter(User.transaction_id.in_([transaction_id])).all():
        g = session.query(User).where(User.transaction_id == transaction_id).all()
        session.close()
        if len(g) < 2 and g[0].metod != metod:
            return True
        else:
            return False
    else:
        session.close()
        return True


async def get_settings():
    # выборка из settings
    session = Session()
    wallet = session.query(Settings.wallet).all()
    group = session.query(Settings.group).all()
    session.close()
    return wallet, group


async def updete_settings(adress):
    # запись суммы в settings
    session = Session()
    try:
        obj = session.query(User).where(User.own_adress == adress).order_by(User.id.desc()).first()
        session.query(Settings).filter(Settings.wallet == adress).update({Settings.balance: obj.balance})
        session.commit()
    except:
        session.rollback()
        print("Ошибка добавления в БД")
    session.close()
