from sql_alchemy import cread_table, cend_message_all_transactions, commit_message, proverka, updete_settings
from datetime import datetime
import time
import asyncio
from aiohttp import ClientSession


async def parser(adress, block_timestamp):
    data = await multi_recuest(adress, block_timestamp)
    h = (data.get("meta"))
    if (h.get("fingerprint")):
        data_full = True
    else:
        data_full = False
    try:
        balance = await summa_get(adress)
    except:
        balance = None
    if balance == None:
        balance = 0
    for i in range(len(data["data"])):
        transaction_id = data["data"][i]["transaction_id"]
        block_timestamp_d = data["data"][i]["block_timestamp"]
        from_vallet = data["data"][i]["from"]
        to_vallet = data["data"][i]["to"]
        value = int(data["data"][i]["value"]) / 1000000
        value = float('{:.2f}'.format(value))
        time = f'{str(datetime.fromtimestamp(float(str(data["data"][i]["block_timestamp"])[:-3]) + 14400))}+04:00'
        usdt = None
        own_adress = adress
        if own_adress.lower() == from_vallet.lower():
            metod = "sending"
        else:
            metod = "admission"
        try:
            if data["data"][i]["token_info"]["address"] == 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t':
                usdt = True
        except:
            usdt = False
        blockchain = 'trx'
        name_token = 'usdt'
        rate = ''
        bot_set = ''
        currency = ''
        purpose = ''
        if block_timestamp != block_timestamp_d and usdt and value > 1 and data["data"][i]["value"] != '115792089237316195423570985008687907853269984665640564039457584007913129639935':
            if await proverka(transaction_id, metod):
                await cend_message_all_transactions(time, value, name_token, balance, from_vallet, to_vallet, metod, transaction_id, rate, currency, purpose, own_adress, blockchain, str(block_timestamp_d)[:-3], bot_set)
                await updete_settings(adress)
                print(time, value, name_token, balance, from_vallet, to_vallet, metod, transaction_id, rate, currency, purpose, own_adress, blockchain, str(block_timestamp_d)[:-3], bot_set, block_timestamp_d)
    try:
        block_timestamp = data["data"][len(data["data"])-1]["block_timestamp"]
    except:
        block_timestamp = 0
    if data_full:
        await parser(adress, block_timestamp)
    return block_timestamp


async def summa_get(adress):
    url = (f'https://api.trongrid.io/v1/accounts/{adress}')
    headers = {'Content-Type': "application/json", 'TRON-PRO-API-KEY': "781c7e24-dc98-4563-9806-ed4fdb57985c"}
    async with ClientSession() as session:
        async with session.get(url, headers=headers, ssl=False) as f:
            data = await f.json()
    for i in range (len(data["data"][0]["trc20"])):
        summa_rec = data["data"][0]["trc20"][i]
        if summa_rec.get('TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t') != None:
            summa = float(summa_rec.get('TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t')) / 1000000
            summa = float('{:.2f}'.format(summa))
            return summa


async def multi_recuest(adress, block_timestamp):
    url = (f'https://api.trongrid.io/v1/accounts/{adress}/transactions/trc20?only_confirmed=true&limit=200&order_by=block_timestamp%2Casc&min_timestamp={block_timestamp}')
    headers = {'Content-Type': "application/json", 'TRON-PRO-API-KEY': "559b3f7a-c9fe-40e5-a003-309c9e512b6c"}
    async with ClientSession() as session:
        async with session.get(url, headers=headers, ssl=False) as f:
            data = await f.json()
    return data


async def parser_while(adress, d):
    x = 0
    for i in adress:
        block_timestamp = await parser(i, d[i])
        #await commit_message()
        d[i] = block_timestamp
        print(x)
        x = x + 1
    return d