from sql_alchemy import get_settings
import asyncio
from aiohttp import ClientSession


async def valid_adress():
    trx, eth, bnb = [], [], []
    wallet, group = await get_settings()
    for i in range(len(wallet)):
        if group[i][0] == 'TRC':
            trx.append(wallet[i][0])
        if group[i][0] == 'ERC':
            eth.append(wallet[i][0])
        if group[i][0] == 'BSH':
            bnb.append(wallet[i][0])
    return trx, eth, bnb





