FROM python:3.11

WORKDIR /app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN chmod ugo+x ./app-entrypoint.sh ./main.py ./main_eth.py

ENTRYPOINT [ "./app-entrypoint.sh" ]
