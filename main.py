#!/usr/bin/python

from datetime import datetime
import time
import asyncio
from aiohttp import ClientSession
from sql_alchemy import cread_table, commit_message
from parser_tron import parser_while
from get_settings import valid_adress


async def main():
    trx, eth, bnb = await valid_adress()
    adress = trx
    d = {}
    for i in adress:
        d[i] = 0
    await cread_table()
    print(len(adress))
    while True:
        try:
            if trx != []:
                start_time = datetime.now()
                d = await parser_while(adress, d)
                print(datetime.now() - start_time)
        except:
            d = {}
            for i in adress:
                d[i] = 0
            continue


if __name__ == '__main__':
    asyncio.run(main())
